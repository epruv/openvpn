#!/bin/bash

export BASE='ibacalu/openvpn'
export MAJOR='0'
export MINOR=$(cat ./version | cut -d'.' -f2 || 0)
export VERSION="v${MAJOR}.$((MINOR + 1))"
export BUILD="${VERSION}"
export TAG="${BASE}:${BUILD}"
export LATEST="${BASE}"

echo '#################################################################################'
echo "  Building: ${TAG}"
echo '#################################################################################'
docker build -t ${LATEST} -f Dockerfile .

[[ ! $? -eq 0 ]] && echo "ERROR: Failed to build!" && exit 1

echo
echo '#################################################################################'
echo "  Pushing: ${TAG}"
echo '#################################################################################'
docker tag ${LATEST} ${TAG}
docker push ${LATEST}

[[ ! $? -eq 0 ]] && echo "ERROR: Failed to push!" && exit 1

echo ${VERSION} > version