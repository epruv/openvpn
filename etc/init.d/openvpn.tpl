{{ with printf "%s/%s" (env "VAULT_PATH") (env "CONFIG") | secret }}
#!/bin/ash

# Alter default interface(hack-ish, I know)
if [[ "{{ .Data.data.protocol }}" == "tcp" ]]; then
    DEV_INT="tun0"
else
    DEV_INT="tun1"
fi

# Get Address & Netmask
export $(ipcalc -n {{ .Data.data.network }})
export $(ipcalc -m {{ .Data.data.network }})

# Run OpenVPN
/usr/sbin/openvpn \
  --cd /openvpn \
  --proto {{ .Data.data.protocol }} \
  --port {{ .Data.data.port }} \
  --dev ${DEV_INT} \
  --server ${NETWORK} ${NETMASK} \
  --keepalive 10 60 \
  --push 'redirect-gateway def1' \
  --push 'dhcp-option DNS {{ .Data.data.dns }}' \
  --auth-user-pass-verify ./login.py via-env \
  --duplicate-cn \
  --script-security 3 \
  --verify-client-cert none \
  --username-as-common-name \
  --tls-server \
  --cipher AES-256-CBC \
  --dh keys/dh2048.pem \
  --ca keys/ca.crt \
  --cert keys/server.crt \
  --key keys/server.key
{{ end }}