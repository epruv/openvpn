# **OpenVPN** with **Docker** and Hashicorp's **Vault**


Requirements:
 * Vault server installed (https://www.hashicorp.com/products/vault)
 * Clone GIT Repo
    ```bash
    git clone git@gitlab.com:qts.cloud/docker/openvpn.git
    cd openvpn
    ```
 * Configure Vault secrets/
    * Create `.secrets` file
      ```bash
      # Vault Config
      export VAULT_TOKEN='s.XXXXXXXXX'
      export VAUTL_ADDR='http://my-vault-cname:8200'
      export VAULT_KV_PATH='secret'

      # OpenVPN Config
      export STAGE='dev'
      export CNAME='stage.vpn.my-domain.com'
      export NETWORK='10.42.0.0/24'
      export PORT='1195'
      export PROTOCOL='tcp'
      export PUBLIC_IP='vpn-server-ip'
      ```
    * Configure easyrsa_vars
    * Configure & Run `bash ./generate_certs.sh`
  * Configure docker-compose.yml
    * source `.secrets`
    * deploy
      ```yaml
      version: "3"
      services:
        vpn:
          image: ibacalu/openvpn:v0.3
          environment:
            STAGE: dev
            VAULT_TOKEN: ${VAULT_TOKEN}
            VAULT_ADDR: ${VAULT_ADDR}
          network_mode: host
          restart: always
          cap_add:
            - NET_ADMIN
            - NET_RAW
          devices:
            - "/dev/net/tun"
      ```
  * Create users
    * make sure you have vault installed
    * Enable auth userpass backend
      ```bash
      source .secrets
      vault write auth/openvpn/users/john.doe \
                  password=My-Secret-Password \
                  policies=${STAGE}
      ```

### Default Parameters
```sh
# You may want to use different CONFIG-urations for same Environment. For example both a TCP & UDP server.
export CONFIG=${CONFIG:-config}
export STAGE=${STAGE:-dev}
export VAULT_ADDR=${VAULT_ADDR:-none}
export VAULT_TOKEN=${VAULT_TOKEN:-none}
export VAULT_SKIP_VERIFY=${VAULT_SKIP_VERIFY:-true}
# This is custom based on KV engine version
#   Syntax (v1): VAULT_KV_PATH=${AUTH-ENDPOINT-PATH}
#   Syntax (v2): VAULT_KV_PATH=${AUTH-ENDPOINT-PATH}/data
# NOTE: `/data` is only required when reading secrets
export VAULT_KV_PATH=${VAULT_KV_PATH:-secret/data}
```