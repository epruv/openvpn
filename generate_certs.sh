#!/bin/bash

# VARS
# Load secret ENV Vars if they exist
[[ -f ".secrets" ]] && source .secrets || true

# Environment Config
export STAGE=${STAGE:-dev}
export CONFIG=${CONFIG:-config}

export CNAME=${CNAME:-none}
export DNS=${DNS:-8.8.8.8}
export IPTABLES='iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -t nat -A POSTROUTING -o ${DEV_INT} -j MASQUERADE
iptables -A INPUT -i eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i ${DEV_INT} -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -j ACCEPT'
export NETWORK=${NETWORK:-10.42.0.0/24}
export PORT=${PORT:-1195}
export PROTOCOL=${PROTOCOL:-udp}
export PUBLIC_IP=${PUBLIC_IP:-PROVIDE_PUBLIC_IP}

export VAULT_ADDR=${VAULT_ADDR:-none}
export VAULT_TOKEN=${VAULT_TOKEN:-none}
export VAULT_SKIP_VERIFY=${VAULT_SKIP_VERIFY:-true}
export VAULT_KV_PATH=${VAULT_KV_PATH:-kv}
export VAULT_PATH="${VAULT_KV_PATH}/${STAGE}/openvpn"


# EASY_RSA CONFIG
OS="nix"
EasyRSA_VER='3.0.5'
EasyRSA_FILE="EasyRSA-${OS}-${EasyRSA_VER}"
WORKDIR="/tmp/EasyRSA"
EasyRSA_TGZ="${EasyRSA_FILE}.tgz"
EasyRSA_URL="https://github.com/OpenVPN/easy-rsa/releases/download/v${EasyRSA_VER}/${EasyRSA_TGZ}"

if [[ -f "./easyrsa_vars" ]]; then
    EasyRSA_VARS="easyrsa_vars"
else
    EasyRSA_VARS="${WORKDIR}/vars.example"
fi


# Cleanup
[[ -d "${WORKDIR}" ]] && rm -rf "${WORKDIR}" || true
mkdir "${WORKDIR}"

# Download & Extract EasyRSA
echo "Downloading EasyRSA..."
wget --quiet ${EasyRSA_URL} && tar -C "${WORKDIR}" --strip-components=1 -zxf "${EasyRSA_TGZ}" && rm -f "${EasyRSA_TGZ}"

cp "${EasyRSA_VARS}" "${WORKDIR}/vars"
cd "${WORKDIR}"

# Initialise
echo "Initialising..."
./easyrsa init-pki

# Build CA
echo "Building CA..."
./easyrsa build-ca nopass

# Generate Server CSR
echo "Generating Server CSR..."
./easyrsa gen-req server nopass

# Import & Sign Server CSR
echo "Signing Server CSR..."
yes | ./easyrsa sign-req server server

# Generate DH
echo "Generating Diffie-Hellman key exchange..."
./easyrsa gen-dh

# Save to VAULT
read -p "Save to VAULT(y/N)? " choice
case "$choice" in
    y|Y|yes ) echo -ne "\t Saving to vault...\n";;
    n|N|no ) exit;;
    * ) exit;;
esac


### Save to VAULT

# Old style
# read -p "${VAULT_PATH}/[ca].crt: " input
# cacrt_path="${input:-ca}.crt"
# vault kv put ${VAULT_PATH}/${cacrt_path} value=@pki/ca.crt

# Save Certs
vault kv put ${VAULT_PATH}/ca.crt value=@pki/ca.crt
vault kv put ${VAULT_PATH}/ca.key value=@pki/private/ca.key
vault kv put ${VAULT_PATH}/server.crt value=@pki/issued/server.crt
vault kv put ${VAULT_PATH}/server.key value=@pki/private/server.key
vault kv put ${VAULT_PATH}/server.csr value=@pki/reqs/server.req
vault kv put ${VAULT_PATH}/dh2048.pem value=@pki/dh.pem

# Save Server Config
vault kv put ${VAULT_PATH}/${CONFIG} cname="${CNAME}" dns="${DNS}" iptables="${IPTABLES}" network="${NETWORK}" port="${PORT}" protocol="${PROTOCOL}" public_ip="${PUBLIC_IP}"
