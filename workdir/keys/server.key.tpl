{{ with printf "%s/server.key" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
