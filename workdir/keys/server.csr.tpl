{{ with printf "%s/server.csr" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
