{{ with printf "%s/ca.crt" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
