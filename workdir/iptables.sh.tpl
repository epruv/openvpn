#!/bin/ash

IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)

{{ with printf "%s/%s" (env "VAULT_PATH") (env "CONFIG") | secret }}
if [[ "{{ .Data.data.protocol }}" == "tcp" ]]; then
    DEV_INT="tun0"
else
    DEV_INT="tun1"
fi
{{ .Data.data.iptables }}
{{ end }}