# OpenVPN configuration

client
proto udp
{{ with printf "%s/%s" (env "VAULT_PATH") (env "CONFIG") | secret }}
remote {{ .Data.data.cname }} {{ .Data.data.port }}
{{ end }}
dev tun
tls-client
remote-cert-tls server
auth-user-pass
cipher AES-256-CBC

<ca>
{{ with printf "%s/ca.crt" (env "VAULT_PATH") | secret }}
{{ .Data.data.value }}
{{ end }}
</ca>
