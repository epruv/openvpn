#!/bin/ash


### STAGE Variables
export WORKDIR=${WORKDIR:-/openvpn}
export CONFIG=${CONFIG:-config}
export STAGE=${STAGE:-dev}


# Vault Config
export VAULT_ADDR=${VAULT_ADDR:-none}
export VAULT_TOKEN=${VAULT_TOKEN:-none}
export VAULT_SKIP_VERIFY=${VAULT_SKIP_VERIFY:-true}
export VAULT_KV_PATH=${VAULT_KV_PATH:-secret/data}
export VAULT_PATH="${VAULT_KV_PATH}/${STAGE}/openvpn"

cd $WORKDIR
echo "-> Loading config from Vault..."
echo "      VAULT_ADDR: ${VAULT_ADDR}"
echo "      Config:     ${CONFIG}"
echo "      STAGE:      ${STAGE}"
[[ -z "${VAULT_ADDR}" ]] && exit 1 || echo "-> Configuring OpenVPN Server"


echo "-> Configure iptables rules..."
consul-template -config ${WORKDIR}/consul.hcl -template "${WORKDIR}/iptables.sh.tpl:${WORKDIR}/iptables.sh" -once
chmod +x ${WORKDIR}/iptables.sh
${WORKDIR}/iptables.sh

echo "-> Configure OpenVPN certs..."
Certs='ca.crt dh2048.pem server.crt server.key'
for cert in $Certs;
do
    consul-template -config ${WORKDIR}/consul.hcl -template "${WORKDIR}/keys/${cert}.tpl:${WORKDIR}/keys/${cert}" -once
done

echo "-> Configure OpenVPN service..."
consul-template -config ${WORKDIR}/consul.hcl -template "/etc/init.d/openvpn.tpl:/etc/init.d/openvpn" -once

echo "-> Configure OpenVPN client..."
consul-template -config ${WORKDIR}/consul.hcl -template "${WORKDIR}/client.ovpn.tpl:${WORKDIR}/client.ovpn" -once

echo "-> Configure Login client..."
consul-template -config ${WORKDIR}/consul.hcl -template "${WORKDIR}/login.py.tpl:${WORKDIR}/login.py" -once
chmod +x ${WORKDIR}/login.py

echo "-> Starting OpenVPN..."
chmod +x /etc/init.d/openvpn
/etc/init.d/openvpn
