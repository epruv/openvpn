FROM python:alpine

WORKDIR /openvpn

# Add files
ADD workdir .
ADD bin/consul-template /usr/bin/consul-template
ADD /etc/init.d/openvpn.tpl /etc/init.d/openvpn.tpl

# Install requirements
RUN apk update
RUN apk add openvpn iptables

RUN echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
RUN chmod +x /usr/bin/consul-template
RUN chmod +x entrypoint.sh

RUN pip install --no-cache-dir -r requirements.txt

CMD ["/bin/ash", "/openvpn/entrypoint.sh"]